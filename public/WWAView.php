<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

    <title>About us</title>
</head>

<body>
<div id=header>
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand text-success" href="Accueil.php">Eventflex</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Accueil.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Profile.php">Mon profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche par date</a>
                    </li>
                    <li>
                        <a class="nav-link active" aria-current="page" href="AmisView.php">Amis</a>
                    </li>
                    <li>
                            <a class="nav-link active" aria-current="page" href="AdminView.php">Admin</a>
                        </li>
                    <li>
                        <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Plus...
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#">Qui sommes-nous ?</a></li>
                            <li><a class="dropdown-item" href="#">Déconnexion</a></li>
                        </ul>
                    </li>
                </ul>
                <a class="btn btn-outline-success w-100 me-3 ms-auto" style="max-width:10%" href="#">Nouvelle publication</a>
            </div>
        </div>
    </div>
    <div class="container col-md-5 ">
        <div>
            <h2 id=title class="col-md-12 mt-5 text-center mb-5">Qui sommes-nous ?</h2>
            <p class="col-md-12 mt-5 text-center mb-5">Nous sommes des étudiants du BUT Informatique de Nice</p>
            <p class="col-md-12 mt-5 text-center mb-5">Notre projet est de créer un réseau social</p>
            <p class="col-md-12 mt-5 text-center mb-5">Notre équipe est composée de :</p>
            <ul class="col-md-12 mt-5 text-center mb-5">DA COSTA Tom </ul>
            <ul class="col-md-12 mt-5 text-center mb-5">GRIFFONNET Matthieu</ul>
            <p class="col-md-12 mt-5 text-center mb-5">Dans le cadre de la ressource R301 dirigé par : </p>
            <ul class="col-md-12 mt-5 text-center mb-5">Mr. Acundeger</ul>
        </div>
    </div>

    <script src="../script/script.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>
