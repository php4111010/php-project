<?php
class DataBase
{
    private $servername = "linserv-info-01.campus.unice.fr";
    private $username = "gm203447";
    private $password = "gm203447";
    private $dbname = "gm203447_R301";
    private $conn;

    public function getConnection()
    {
        $this->conn = null;

        try {
            $this->conn = new PDO("mysql:host=" . $this->servername . ";dbname=" . $this->dbname, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }
        return $this->conn;
    }
}
