<?php 
require_once(dirname(__DIR__) . '/controllers/loginController.php');
session_start(); 

$loginController = new LoginController();

$return = $loginController->logout();
if($return){
    echo "User disconnected";
}

if (!isset($_SESSION['email']) || !isset($_SESSION['email'])) {
    header("Location: loginView.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Logout</title>
</head>
<body>



</body>
</html>