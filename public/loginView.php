<?php
require_once(dirname(__DIR__) . '/controllers/LoginController.php');
session_start();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="../style.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>Login</title>
</head>

<body>
<div id=header>
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand text-success" href="Accueil.php">Eventflex</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Accueil.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Profile.php">Mon profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche par date</a>
                    </li>
                    <li>
                        <a class="nav-link active" aria-current="page" href="AmisView.php">Amis</a>
                    </li>
                    <li>
                            <a class="nav-link active" aria-current="page" href="AdminView.php">Admin</a>
                        </li>
                    <li>
                        <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Plus...
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="WWAView.php">Qui sommes-nous ?</a></li>
                            <li><a class="dropdown-item" href="logoutView.php">Déconnexion</a></li>
                        </ul>
                    </li>
                </ul>
                <a class="btn btn-outline-success w-100 me-3 ms-auto" style="max-width:10%" href="#">Nouvelle publication</a>
            </div>
        </div>
    </div>

    <div class="container col-md-5 ">
        <div>
            <h2 id=title class="col-md-12 mt-5 text-center mb-5">Connexion</h2>
            <?php if (isset($error)) { ?>
                <p>
                    <?php echo $error; ?>
                </p>
            <?php } ?>

            <form action="" method="POST" enctype="multipart/form-data" class="row">
                <div class="col-md-10">
                    <label class="form-label" for="username" id=requirment>Email</label>
                    <input class="form-control" type="email" id="email" name="email" required value="<?php echo isset($_COOKIE['email']) ? $_COOKIE['email'] : ''; ?>">

                </div>
                <div class="col-md-10">
                    <label class="form-label mt-3" for="password" id=requirment>Mot de passe</label>
                    <input class="form-control" type="password" id="password" name="password" required>
                </div>
                <div class="col-md-12 mt-5">
                    <input class="btn btn-primary" type="submit" id=Connexion value="Se connecter">
                </div>
            </form>
        </div>
        <div id="DC" class="col-md-12 mt-5 text-center">
            <p id=Login>Pas encore de compte ? <a href="Formulaire.php">Inscrivez-vous.</a></p>
        </div>
    </div>
</body>

<?php
// Simulation des actions après soumission du formulaire
$loginController = new LoginController();
if (isset($_POST['email']) && isset($_POST['password'])) {
    $return = $loginController->login();
    if ($return) {
        echo "Connexion réussie !";
        $_SESSION['email'] = $_POST['email'];
        $_SESSION['password'] = $_POST['password'];

        $email = $_POST["email"];
        $pswd = $_POST["password"];

        // Définir les cookies pour l'email et le mot de passe
        setcookie("email", $email, time() + (86400 * 30)); // 86400 = 1 jour
        setcookie("password", $pswd, time() + (86400 * 30));

        header("Location: Accueil.php");
    }
}
?>

</html>
