document.querySelectorAll('.zoomable').forEach(function (card) {
    card.addEventListener('click', function () {
        document.getElementById('modal-email').textContent = this.dataset.email;
        document.getElementById('modal-nom').textContent = this.dataset.nom;
        document.getElementById('modal-prenom').textContent = this.dataset.prenom;
        document.getElementById('modal-naissance').textContent = this.dataset.naissance;
        document.getElementById('modal-telephone').textContent = this.dataset.telephone;
        document.getElementById('modal-adresse').textContent = this.dataset.adresse;
    });
});
document.querySelectorAll('.zoomableimg').forEach(item => {
    item.addEventListener('click', function () {
        const modalImage = document.getElementById('modal-image');
        modalImage.src = this.src;
    });
});