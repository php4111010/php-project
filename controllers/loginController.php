<?php
require_once(dirname(__DIR__) . '/models/UserModel.php');

class LoginController
{
    private $userModel;

    public function index()
    {
        require_once 'login_view.php';
    }

    public function __construct()
    {
        $this->userModel = new UserModel();
    }

    public function login()
    {
        $username = $_POST['email'];
        $password = $_POST['password'];

        $userModel = $this->userModel;
        if ($userModel->checkCredentials($username, $password)) {
            $_SESSION['user'] = $username;
            return true;
        } else {
            echo "Invalid username or password";
        }
    }

    public function register($path)
    {
        $username = $_POST['email'];
        $password = $_POST['password'];
        $photo = $path;
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $naissance = $_POST['naissance'];
        $adresse = $_POST['adresse'];
        $telephone = $_POST['telephone'];

        $userModel = $this->userModel;

        $result = $userModel->createUser($username, $password, $photo, $nom, $prenom, $naissance, $adresse, $telephone);
        if($result == true){
            echo "User created";
            return true;
        }else if($result == false){
            echo "User already exists";
            return false;
        }
    }

    public function logout()
    {
        session_destroy();
        header("Location: loginView.php");
    }

    public function destruct()
    {
        $this->userModel = null;
    }
}
