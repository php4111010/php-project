<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>Amis</title>
</head>

<body>
<div id=header>
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand text-success" href="Accueil.php">Eventflex</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Accueil.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Profile.php">Mon profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche par date</a>
                    </li>
                    <li>
                        <a class="nav-link active" aria-current="page" href="AmisView.php">Amis</a>
                    </li>
                    <li>
                            <a class="nav-link active" aria-current="page" href="AdminView.php">Admin</a>
                        </li>
                    <li>
                        <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Plus...
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="WWAView.php">Qui sommes-nous ?</a></li>
                            <li><a class="dropdown-item" href="logoutView.php">Déconnexion</a></li>
                        </ul>
                    </li>
                </ul>
                <a class="btn btn-outline-success w-100 me-3 ms-auto" style="max-width:10%" href="#">Nouvelle publication</a>
            </div>
        </div>
    </div>
    <div class="container">
        <h1>Amis </h1>

        <?php
        // Simulation des données des utilisateurs
        $allUsers = array(
            array(
                'email' => 'user1@example.com',
                'nom' => 'Nom1',
                'prenom' => 'Prénom1'
            ),
            array(
                'email' => 'user2@example.com',
                'nom' => 'Nom2',
                'prenom' => 'Prénom2'
            )
        );

        // Simuler les données d'amis, demandes et attentes
        $amis = array(
            array('emailAmi' => 'ami1@example.com'),
            array('emailAmi' => 'ami2@example.com')
        );

        $demandes = array(
            array('email' => 'demande1@example.com'),
            array('email' => 'demande2@example.com')
        );

        $attentes = array(
            array('emailAmi' => 'attente1@example.com'),
            array('emailAmi' => 'attente2@example.com')
        );

        if (isset($_POST['action'])) {
            $action = explode("_", $_POST['action']);
            if ($action[0] == "add") {
                // Action ajouter un ami simulée
                echo "<p>Ami ajouté : {$action[1]}</p>";
            } else if ($action[0] == "accept") {
                // Action accepter une demande simulée
                echo "<p>Demande d'ami acceptée : {$action[1]}</p>";
            } else if ($action[0] == "annuler") {
                // Action annuler une demande simulée
                echo "<p>Demande d'ami annulée : {$action[1]}</p>";
            } else if ($action[0] == "refuser") {
                // Action refuser une demande simulée
                echo "<p>Demande d'ami refusée : {$action[1]}</p>";
            }
        }
        ?>

        <br>
        <h2>Mes amis :</h2>
        <?php
        foreach ($amis as $ami) {
            echo "<label for='emailAmi'>{$ami['emailAmi']}</label><br>";
        }
        ?>

        <br>
        <h2>Ajouter un ami :</h2>
        <?php
        foreach ($allUsers as $user) {
            if (in_array($user['email'], array_column($amis, 'emailAmi')) || in_array($user['email'], array_column($demandes, 'email')) || in_array($user['email'], array_column($attentes, 'emailAmi'))) {
                continue;
            }
            echo "<form action='' method='post'>";
            echo "<label for='emailAmi'>{$user['email']}</label>";
            echo "<button type='submit' name='action' value='add_{$user['email']}'>Ajouter</button>";
            echo "</form>";
        }
        ?>

        <br>
        <h2>Accepter une demande d'ami :</h2>
        <?php
        foreach ($demandes as $demande) {
            echo "<form action='' method='post'>";
            echo "<label for='emailAmi'>{$demande['email']}</label>";
            echo "<button type='submit' name='action' value='accept_{$demande['email']}'>Accepter</button>";
            echo "<button type='submit' name='action' value='refuser_{$demande['email']}'>Refuser</button>";
            echo "</form>";
        }
        ?>

        <br>
        <h2>En attente d'acceptation :</h2>
        <?php
        foreach ($attentes as $attente) {
            echo "<label for='emailAmi'>{$attente['emailAmi']}</label><br>";
            echo "<form action='' method='post'>";
            echo "<button type='submit' name='action' value='annuler_{$attente['emailAmi']}'>Annuler</button>";
        }
        ?>

    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>

</html>
