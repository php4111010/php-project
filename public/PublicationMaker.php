<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Publication maker</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
<div id=header>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container-fluid">
        <a class="navbar-brand text-success" href="Accueil.php">Eventflex</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="Accueil.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="Profile.php">Mon profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche par date</a>
                </li>
                <li>
                    <a class="nav-link active" aria-current="page" href="AmisView.php">Amis</a>
                </li>
                <li>
                        <a class="nav-link active" aria-current="page" href="AdminView.php">Admin</a>
                    </li>
                <li>
                    <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Plus...
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="WWAView.php">Qui sommes-nous ?</a></li>
                        <li><a class="dropdown-item" href="logoutView.php">Déconnexion</a></li>
                    </ul>
                </li>
            </ul>
            <a class="btn btn-outline-success w-100 me-3 ms-auto" style="max-width:10%" href="#">Nouvelle publication</a>
        </div>
    </div>
</div>
<div class="container">
    <h1 class="label mt-3">Nouvelle Publication</h1>
    <form action="" method="post" enctype="multipart/form-data">
        <label for="type">Type de publication</label>
        <select select class="form-select" id="floatingSelect" aria-label="Floating label select example" name="type">
            <option value="text">Texte</option>
            <option value="image">Image</option>
        </select>
        <button type="submit" name="action" class="btn btn-primary mt-3" value="select">Sélectionner</button>
    </form>
</div>
</body>
</html>
