<?php
// Simuler l'inclusion des contrôleurs
function __autoload($class_name) {
    require_once dirname(__DIR__) . '/controllers/' . $class_name . '.php';
}

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Simulation des données utilisateur
$_COOKIE["email"] = "user@example.com";

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--<link rel="stylesheet" href="../style.css">-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>Formulaire inscription</title>
</head>

<body>
<div id=header>
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand text-success" href="Accueil.php">Eventflex</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Accueil.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Profile.php">Mon profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche par date</a>
                    </li>
                    <li>
                        <a class="nav-link active" aria-current="page" href="AmisView.php">Amis</a>
                    </li>
                    <li>
                            <a class="nav-link active" aria-current="page" href="AdminView.php">Admin</a>
                        </li>
                    <li>
                        <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Plus...
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="WWAView.php">Qui sommes-nous ?</a></li>
                            <li><a class="dropdown-item" href="logoutView.php">Déconnexion</a></li>
                        </ul>
                    </li>
                </ul>
                <a class="btn btn-outline-success w-100 me-3 ms-auto" style="max-width:10%" href="#">Nouvelle publication</a>
            </div>
        </div>
    </div>
    <div class="container col-md-5 ">
        <div>
            <h2 id=title class="col-md-12 mt-3 text-center mb-5">Creation de compte</h2>
            <form action="" method="POST" enctype="multipart/form-data" class="row">
                <div class="col-md-6">
                    <label class="form-label" for="email" id="text">Email</label><br>
                    <input class="form-control" type="email" id="input" name="email" placeholder="Votre email" required="required" value="<?php echo $_COOKIE['email']; ?>"><br>
                </div>
                <!-- Ajoutez ici les autres champs du formulaire -->
                <div class="col-md-12">
                    <input class="btn btn-primary" class type="submit" value="Envoyer">
                </div>
            </form>
        </div>
        <div id="DC" class="col-md-12 mt-3 text-center">
            <p id="Login">Vous avez déjà un compte ? <a href="loginView.php">Connectez-vous</a>.</p>
        </div>
    </div>
</body>

<?php
// Simulation des actions après soumission du formulaire
$loginController = new LoginController();
$imageController = new ImageController();

// Simuler les données POST
$_POST['email'] = "user@example.com";
$_POST['password'] = "password";
$_POST['nom'] = "Nom";
$_POST['prenom'] = "Prénom";
$_FILES['photo']['name'] = "photo.jpg";
$_POST['naissance'] = "1990-01-01";
$_POST['adresse'] = "123 Rue des Fleurs";
$_POST['telephone'] = "0123456789";

if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['nom']) 
    && isset($_POST['prenom']) && isset($_FILES['photo']) && isset($_POST['naissance']) 
    && isset($_POST['adresse']) && isset($_POST['telephone'])) {
    $file_path = $imageController->upload();
    if($file_path != null){
        $return = $loginController->register($file_path);
        if ($return) {
            $_SESSION['email'] = $_POST['email'];
            $_SESSION['password'] = $_POST['password'];
            $email = $_POST["email"];
            $pswd = $_POST["password"];

            // Définir les cookies pour l'email et le mot de passe
            setcookie("email", $email, time() + (86400 * 30)); // 86400 = 1 jour
            setcookie("password", $pswd, time() + (86400 * 30));

            header("Location: loginView.php");
        }else{
            $imageController->delete($file_path);
        }
    }    
}
?>
</html>
