<?php
require_once('DataBase.php');

class AdminModel{
    private $db;

    public function __construct(){
        $this->db = new Database();
    }

    public function isAdmin($email){
        $query = "SELECT * FROM Utilisateur WHERE email = :email";
        $stmt = $this->db->getConnection()->prepare($query);
        $stmt->bindParam(":email", $email);
        $stmt->execute();

        if($stmt->rowCount() < 0){
            echo "Email doesn't existe";
            return false;
        }

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (isset($row['estAdmin'])) {
            return $row['estAdmin'] == 1;
        } else {
            echo "Email doesn't exist";
            return false;
        }
    }

    public function blockUser($email)
    {
        $query = "UPDATE Utilisateur SET estBloque = 1 WHERE email = :email";
        $stmt = $this->db->getConnection()->prepare($query);
        $stmt->bindParam(":email", $email);
        $stmt->execute();
    }

    public function deleteUser($email)
    {
        $query = "DELETE FROM Utilisateur WHERE email = :email";
        $stmt = $this->db->getConnection()->prepare($query);
        $stmt->bindParam(":email", $email);
        $stmt->execute();
    }

    public function isBlocked($email)
    {
        $query = "SELECT * FROM Utilisateur WHERE email = :email";
        $stmt = $this->db->getConnection()->prepare($query);
        $stmt->bindParam(":email", $email);
        $stmt->execute();

        if($stmt->rowCount() < 0){
            echo "Email doesn't existe";
            return false;
        }

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (isset($row['estBloque'])) {
            return $row['estBloque'] == 1;
        } else {
            echo "Email doesn't exist";
            return false;
        }
    }

    public function unblockUser($email)
    {
        $query = "UPDATE Utilisateur SET estBloque = 0 WHERE email = :email";
        $stmt = $this->db->getConnection()->prepare($query);
        $stmt->bindParam(":email", $email);
        $stmt->execute();
    }
}
?>