<?php
require_once('DataBase.php');

class PublicationModel{
    private $db;
    private $titre;
    private $description;
    private $photo;


    public function __construct(){
        $this->db = new DataBase();
    }

    public function createPublication($titre, $photo, $email, $estPublic, $type){
        $sql = "INSERT INTO Publication (email, idPublication, text, image, estPublic, nbLike, nbDislike, date, type) VALUES (:email, :idPublication, :text, :image, :estPublic, 0 , 0, current_timestamp(), :type)";
        $stmt = $this->db->getConnection()->prepare($sql);
        $id = hash('sha256', $email.$titre.$photo);
        $stmt->bindParam(':idPublication', $id);
        $stmt->bindParam(':text', $titre);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':estPublic', $estPublic);
        $stmt->bindParam(':image', $photo);
        $stmt->bindParam(':type', $type);
        $stmt->execute();
        echo "Publication created";
        return $this->db->getConnection()->lastInsertId();
    }

    public function getPublications(){
        $sql = "SELECT * FROM Publication ORDER BY date DESC";
        $stmt = $this->db->getConnection()->prepare($sql);
        $stmt->execute();
        return  $stmt->fetchAll();
    }

    public function deletePublication($id){
        $sql = "DELETE FROM Publication WHERE idPublication = :id";
        $stmt = $this->db->getConnection()->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }

    public function likePublication($id){
        $sql = "UPDATE Publication SET nbLike = nbLike + 1 WHERE idPublication = :id";
        $stmt = $this->db->getConnection()->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }

    public function dislikePublication($id){
        $sql = "UPDATE Publication SET nbDislike = nbDislike + 1 WHERE idPublication = :id";
        $stmt = $this->db->getConnection()->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }

    public function commenterPublication($id, $commentaire, $idUtilisateur){
        $sql = "INSERT INTO Commentaire (idPublication, utilisateur, commentaire, date) VALUES (:idPublication, :utilisateur, :commentaire,  current_timestamp())";
        $stmt = $this->db->getConnection()->prepare($sql);
        $stmt->bindParam(':idPublication', $id);
        $stmt->bindParam(':commentaire', $commentaire);
        $stmt->bindParam(':utilisateur', $idUtilisateur);
        $stmt->execute();
    }

    public function getCommentaires($id){
        $sql = "SELECT * FROM Commentaire WHERE idPublication = :id ORDER BY date DESC";
        $stmt = $this->db->getConnection()->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return  $stmt->fetchAll();
    }

    public function searchPublicationBetweenDates($date1, $date2)
    {
        $sql = "SELECT DATE_SUB(:date1, INTERVAL 1 DAY)";
        $stmt = $this->db->getConnection()->prepare($sql);
        $stmt->bindParam(':date1', $date1);
        $stmt->execute();
        $date1 = $stmt->fetchColumn();

        $sql = "SELECT DATE_ADD(:date2, INTERVAL 1 DAY)";
        $stmt = $this->db->getConnection()->prepare($sql);
        $stmt->bindParam(':date2', $date2);
        $stmt->execute();
        $date2 = $stmt->fetchColumn();

        $sql = "SELECT * FROM Publication WHERE date BETWEEN :date1 AND :date2";
        $stmt = $this->db->getConnection()->prepare($sql);
        $stmt->bindParam(':date1', $date1);
        $stmt->bindParam(':date2', $date2);
        $stmt->execute();
        return  $stmt->fetchAll();
    }
}
