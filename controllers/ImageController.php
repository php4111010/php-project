<?php
class ImageController
{
    public function upload(){
        if ($_FILES["photo"]["error"] > 0) {
            echo "Error during the upload : " . $_FILES["photo"]["error"] . "<br />";
            return null;
        } else {
            $uploadDir = dirname(__DIR__) . '/images/';
            $uploadFile = $uploadDir . basename($_FILES['photo']['name']);
            if(move_uploaded_file($_FILES['photo']['tmp_name'], $uploadFile)){
                echo "File is valid, and was successfully uploaded.\n";
                return "../images/".$_FILES['photo']['name'];
            } else {
                echo "Error while uploading : " . $_FILES["photo"]["error"] . "<br />";
                return null;
            }
        }
    }

    public function delete($file_path){
        unlink($file_path);
    }
}
?>