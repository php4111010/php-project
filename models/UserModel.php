<?php
require_once('DataBase.php');
class UserModel
{
    private $conn;
    private $table_name = "Utilisateur";

    private $estAdmin;
    private $email;
    private $photo;
    private $nom;
    private $prenom;
    private $naissance;
    private $adresse;
    private $telephone;


    public function __construct()
    {
        $database = new Database();
        $this->conn = $database->getConnection();
    }

    public function checkCredentials($username, $password)
    {
        $query = "SELECT * FROM " . $this->table_name . " WHERE email = :email";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":email", $username);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return password_verify($password, $row['mdp']);
        } else {
            return false;
        }
    }

    public function createUser($email, $mdp, $photo, $nom, $prenom, $naissance, $adresse, $telephone)
    {

        $query = "SELECT * FROM " . $this->table_name . " WHERE email = :email";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":email", $email);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            echo "Username already exists";
            return false;
        }

        $query = "INSERT INTO " . $this->table_name . " (estAdmin, email, mdp, photo, nom, prenom, naissance, adresse, telephone) VALUES (0, :email, :mdp, :photo, :nom, :prenom, :naissance, :adresse, :telephone)";
        
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":email", $email);
        $password = password_hash($mdp, PASSWORD_DEFAULT);
        $stmt->bindParam(":mdp", $password);
        $stmt->bindParam(":photo", $photo);
        $stmt->bindParam(":nom", $nom);
        $stmt->bindParam(":prenom", $prenom);
        $stmt->bindParam(":naissance", $naissance);
        $stmt->bindParam(":adresse", $adresse);
        $stmt->bindParam(":telephone", $telephone);
        $stmt->execute();
        return true;
    }

    public function getUser($email)
    {
        $query = "SELECT * FROM " . $this->table_name . " WHERE email = :email";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":email", $email);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->estAdmin = $row['estAdmin'];
            $this->email = $row['email'];
            $this->photo = $row['photo'];
            $this->nom = $row['nom'];
            $this->prenom = $row['prenom'];
            $this->naissance = $row['naissance'];
            $this->adresse = $row['adresse'];
            $this->telephone = $row['telephone'];
            return $this;
        }
        return null;
    }

    public function getEstAdmin()
    {
        return $this->estAdmin;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getNaissance()
    {
        return $this->naissance;
    }

    public function getAdresse()
    {
        return $this->adresse;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function getAllUsers(){
        $query = "SELECT * FROM " . $this->table_name;
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $users = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $user = new UserModel();
            $user->estAdmin = $row['estAdmin'];
            $user->email = $row['email'];
            $user->photo = $row['photo'];
            $user->nom = $row['nom'];
            $user->prenom = $row['prenom'];
            $user->naissance = $row['naissance'];
            $user->adresse = $row['adresse'];
            $user->telephone = $row['telephone'];
            array_push($users, $user);
        }
        return $users;
    }

}
