<?php
require_once(dirname(__DIR__) . '/models/PublicationModel.php');
require_once(dirname(__DIR__) . '/controllers/ImageController.php');
require_once(dirname(__DIR__) . '/controllers/VideoController.php');
class PublicationController{
    private $publicationModel;
    private $imageController;
    private $videoController;

    public function __construct(){
        require_once(dirname(__DIR__) . '/models/PublicationModel.php');
        $this->publicationModel = new PublicationModel();
        $this->imageController = new ImageController();
        $this->videoController = new VideoController();
    }

    public function createPublication($titre, $photo, $email, $estPublic, $type){
        if($type == "image"){
            $photoPath = $this->imageController->upload();
            echo $photoPath;
            if($photoPath == null) {
                return;
            }
            echo "<br>Start creating publication : Image uploaded";
            $this->publicationModel->createPublication($titre, $photoPath, $email, $estPublic, $type);
        }else if($type == "video"){
            $videoPath = $this->videoController->upload();
            echo $videoPath;
            if($videoPath == null) {
                return;
            }
            echo "<br>Start creating publication : Video uploaded";
            $this->publicationModel->createPublication($titre, $videoPath, $email, $estPublic, $type);
        }else if($type == "text"){
            echo "<br>Start creating publication : Text";
            $this->publicationModel->createPublication($titre, "null", $email, $estPublic, $type);
        }
    }

    public function getPublications(){
        return $this->publicationModel->getPublications();
    }

    public function deletePublication($id){
        $this->publicationModel->deletePublication($id);
    }

    public function likePublication($id){
        $this->publicationModel->likePublication($id);
    }

    public function dislikePublication($id){
        $this->publicationModel->dislikePublication($id);
    }

    public function commenterPublication($id, $commentaire, $idUtilisateur){
        $this->publicationModel->commenterPublication($id, $commentaire, $idUtilisateur);
    }

    public function getCommentaires($id){
        return $this->publicationModel->getCommentaires($id);
    }

    public function searchPublicationBetweenDates($date1, $date2){
        return $this->publicationModel->searchPublicationBetweenDates($date1, $date2);
    }
}
