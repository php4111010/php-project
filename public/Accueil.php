<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>Accueil</title>
</head>

<body>
    <div id=header>
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand text-success" href="Accueil.php">Eventflex</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Accueil.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Profile.php">Mon profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche par date</a>
                    </li>
                    <li>
                        <a class="nav-link active" aria-current="page" href="AmisView.php">Amis</a>
                    </li>
                    <li>
                            <a class="nav-link active" aria-current="page" href="AdminView.php">Admin</a>
                        </li>
                    <li>
                        <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Plus...
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="WWAView.php">Qui sommes-nous ?</a></li>
                            <li><a class="dropdown-item" href="logoutView.php">Déconnexion</a></li>
                        </ul>
                    </li>
                </ul>
                <a class="btn btn-outline-success w-100 me-3 ms-auto" style="max-width:10%" href="#">Nouvelle publication</a>
            </div>
        </div>
    </div>
    <div class="content-container">
        <!-- Publications -->
        <div class="publications-section text-center">
            <?php
            $publications = array(
                array(
                    'type' => 'image',
                    'image' => '../images/1.jpg',
                    'text' => 'Texte de la publication 1',
                    'date' => '2024-03-07',
                    'idPublication' => 1,
                    'nbLike' => 10,
                    'nbDislike' => 5,
                    'email' => 'example1@example.com'
                ),
                array(
                    'type' => 'text',
                    'text' => 'Texte de la publication 2',
                    'date' => '2024-03-08',
                    'idPublication' => 2,
                    'nbLike' => 20,
                    'nbDislike' => 3,
                    'email' => 'example2@example.com'
                )
            );

            foreach ($publications as $publication) {
                echo "<div class='publication-container'>";
                echo "<div class='publication-content container' style='width: 40rem; margin-bottom: 20px;'>";
                if ($publication['type'] == "image") {
                    echo "<img src='" . $publication['image'] . "' style='max-width: 20rem;max-height: 20rem;'class='card-img-top' alt='...'>";
                    echo "<div class='card-body'>";
                    echo "<h5 class='card-title'>" . $publication['text'] . "</h5>";
                } else if ($publication['type'] == "text") {
                    echo "<p class='card-text' style='font-weight:bold;'>" . $publication['text'] . "</p>";
                    echo "<div class='card-body'>";
                }
                echo "<p class='card-text'>" . $publication['date'] . "</p>";
                echo "<form action='' method='POST'>";
                echo "<button style='border:none;' type='submit' name='action' value='like_" . $publication['idPublication'] . "'>👍</button>" . "<span>" . $publication['nbLike'] . "</span>";
                echo "<button style='border:none;' type='submit' name='action' value='dislike_" . $publication['idPublication'] . "'>👎</button>" . "<span>" . $publication['nbDislike'] . "</span>";
                echo "</form>";
                echo "</div>";
                echo "Publié par : " . $publication['email'] . "<br>";
                echo "Publié le : " . $publication['date'] . "<br>";
                echo "Mettre un commentaire : <br>";
                echo "<form action='' method='POST'>";
                echo "<input type='text' name='commentaire' placeholder='Commentaire'>";
                echo "<button style='border:none;' type='submit' name='action' value='commenter_" . $publication['idPublication'] . "'>-></button>";
                echo "</form>";
                echo "</div>";

                // Commentaires simulés
                $commentaires = array(
                    array(
                        'commentaire' => 'Commentaire 1',
                        'utilisateur' => 'Utilisateur 1',
                        'date' => '2024-03-07'
                    ),
                    array(
                        'commentaire' => 'Commentaire 2',
                        'utilisateur' => 'Utilisateur 2',
                        'date' => '2024-03-08'
                    )
                );

                echo "<div class='comment-section card' style='max-height: 400px; overflow-y: auto;'>";
                foreach ($commentaires as $commentaire) {
                    echo "<div class='comment-container container' style='width: 18rem; margin-bottom: 20px;'>";
                    echo "<div class='card-body'>";
                    echo "<p class='card-text comment-text'>" . $commentaire['commentaire'] . "</p>";
                    echo "</div>";
                    echo "Publié par : " . $commentaire['utilisateur'] . "<br>";
                    echo "Publié le : " . $commentaire['date'] . "<br>";
                    echo "</div>";
                }
                echo "</div>";
                echo "</div>";
            }
            ?>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>

</html>
