<?php
require_once(dirname(__DIR__) . '/controllers/loginController.php');
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (isset($_SESSION['email']) && isset($_SESSION['password'])) {
    header("Location: Accueil.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css">
    <title>register</title>
</head>

<body>
    <div id=header>
        <div id=headertitre>
            <h1 id=Titre>Eventflex</h1>
        </div>
        <nav>
            <a href="WWAView.php"> Qui sommes-nous ?</a>
        </nav>
    </div>
    <div id=boxcrea>
        <div id=CDC>
            <h2 id=title>Creation de compte</h2>
            <form action="Formulaire.php" method="POST" id="form">
                <label for="username" id=requirment>Email</label><br><br>
                <input type="email" id="email" name="email" required><br><br>
                <label for="username" id=requirment>Confirmer l'email</label><br><br>
                <input type="email" id="email" name="email" required><br><br>
                <label for="password" id=requirment>Mot de passe</label><br><br>
                <input type="password" id="password" name="password" required><br><br>
                <label for="password" id=requirment>Confirmer le mot de passe</label><br><br>
                <input type="password" id="password" name="password" required><br><br>
                <input type="submit" id=Connexion value="Création du compte">
            </form>
        </div>
        <div id=DC>
            <p id=Login>Vous avez déjà un compte ? <a href="loginView.php">Connectez-vous</a>.</p>
        </div>
    </div>
</body>

</html>