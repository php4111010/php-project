<?php

class  AmiModel
{
    private $db;
    function __construct()
    {
        require_once(dirname(__DIR__) . '/models/DataBase.php');
        $this->db = new DataBase();
        $this->db = $this->db->getConnection();
    }

    public function getAmis($email)
    {
        $sql = "SELECT * FROM Amis WHERE email = :email AND estAccepte = 1";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(['email' => $email]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getAmisEnAttente($email)
    {
        $sql = "SELECT * FROM Amis WHERE email = :email AND estAccepte = 0";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(['email' => $email]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getAmisDemande($email)
    {
        $sql = "SELECT * FROM Amis WHERE emailAmi = :email AND estAccepte = 0";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(['email' => $email]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function ajouterAmi($email, $emailAmi)
    {
        $sql = "INSERT INTO Amis (email, emailAmi, estAccepte) VALUES (:email, :emailAmi, 0)";
        $stmt = $this->db->prepare($sql);
        try {
            $stmt->execute(['email' => $email, 'emailAmi' => $emailAmi]);
        }catch (PDOException $e){
        }
    }

    public function accepterAmi($email, $emailAmi)
    {
        $sql = "UPDATE Amis SET estAccepte = 1 WHERE email = :email AND emailAmi = :emailAmi";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(['email' => $email, 'emailAmi' => $emailAmi]);
    }

    public function refuserAmi($email, $emailAmi)
    {
        $sql = "DELETE FROM Amis WHERE email = :email AND emailAmi = :emailAmi";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(['email' => $email, 'emailAmi' => $emailAmi]);
    }

    public function supprimerAmi($email, $emailAmi)
    {
        $sql = "DELETE FROM Amis WHERE (email = :email AND emailAmi = :emailAmi) OR (email = :emailAmi AND emailAmi = :email)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(['email' => $email, 'emailAmi' => $emailAmi]);
    }

    public function isMyFriend($email, $emailAmi)
    {
        $sql = "SELECT * FROM Amis WHERE email = :email AND emailAmi = :emailAmi";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(['email' => $email, 'emailAmi' => $emailAmi]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($result) > 0){
            return true;
        }
        return false;
    }

    public function alreadyAsked($email, $emailAmi)
    {
        $sql = "SELECT * FROM Amis WHERE email = :email AND emailAmi = :emailAmi";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(['email' => $email, 'emailAmi' => $emailAmi]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($result) > 0){
            return true;
        }
        return false;
    }


}