<?php
require_once(dirname(__DIR__) ."/models/AdminModel.php");

class AdminController{
    private $model;

    public function __construct(){
        $this->model = new AdminModel();
    }

    public function isAdmin($email){
        return $this->model->isAdmin($email);
    }

    public function blockUser($email)
    {
        $this->model->blockUser($email);
    }

    public function deleteUser($email)
    {
        $this->model->deleteUser($email);
    }

    public function isBlocked($email)
    {
        return $this->model->isBlocked($email);
    }

    public function unblockUser($email)
    {
        $this->model->unblockUser($email);
    }
}

?>