<?php

class AmiController
{
    private $amiModel;

    public function __construct()
    {
        require_once(dirname(__DIR__) . '/models/AmiModel.php');
        $this->amiModel = new AmiModel();
    }

    public function getAmis($email)
    {
        return $this->amiModel->getAmis($email);
    }

    public function getAmisEnAttente($email)
    {
        return $this->amiModel->getAmisEnAttente($email);
    }

    public function getAmisDemande($email)
    {
        return $this->amiModel->getAmisDemande($email);
    }

    public function ajouterAmi($email, $emailAmi)
    {
        $this->amiModel->ajouterAmi($email, $emailAmi);
    }

    public function accepterAmi($email, $emailAmi)
    {
        $this->amiModel->accepterAmi($email, $emailAmi);
    }

    public function refuserAmi($email, $emailAmi)
    {
        $this->amiModel->refuserAmi($email, $emailAmi);
    }

    public function supprimerAmi($email, $emailAmi)
    {
        $this->amiModel->supprimerAmi($email, $emailAmi);
    }

    public function isMyFriend($email, $emailAmi)
    {
        return $this->amiModel->isMyFriend($email, $emailAmi);
    }

    public function alreadySent($email, $emailAmi)
    {
        return $this->amiModel->alreadyAsked($email, $emailAmi);
    }
}