<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>Admin</title>
</head>

<body>
<div id=header>
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand text-success" href="Accueil.php">Eventflex</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Accueil.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Profile.php">Mon profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche par date</a>
                    </li>
                    <li>
                        <a class="nav-link active" aria-current="page" href="AmisView.php">Amis</a>
                    </li>
                    <li>
                            <a class="nav-link active" aria-current="page" href="AdminView.php">Admin</a>
                        </li>
                    <li>
                        <a class="nav-link active" aria-current="page" href="SeachByDates.php">Recherche</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Plus...
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="WWAView.php">Qui sommes-nous ?</a></li>
                            <li><a class="dropdown-item" href="logoutView.php">Déconnexion</a></li>
                        </ul>
                    </li>
                </ul>
                <a class="btn btn-outline-success w-100 me-3 ms-auto" style="max-width:10%" href="#">Nouvelle publication</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <h1 class="col-md-10">Admin</h1>
            <?php 
            // Simulation des données des utilisateurs
            $users = array(
                array(
                    'email' => 'user1@example.com',
                    'nom' => 'Nom1',
                    'prenom' => 'Prénom1',
                    'naissance' => '01/01/1990',
                    'adresse' => 'Adresse1',
                    'telephone' => '123456789'
                ),
                array(
                    'email' => 'user2@example.com',
                    'nom' => 'Nom2',
                    'prenom' => 'Prénom2',
                    'naissance' => '02/02/1991',
                    'adresse' => 'Adresse2',
                    'telephone' => '987654321'
                )
            );

            foreach ($users as $user) {
                echo "<div class='card col-md-4 zoomable' data-bs-toggle='modal' data-bs-target='#control' data-email='{$user['email']}' data-nom='{$user['nom']}' data-prenom='{$user['prenom']}' data-naissance='{$user['naissance']}' data-adresse='{$user['adresse']}' data-telephone='{$user['telephone']}' style='width: 18rem; margin-bottom: 20px; margin-left: 50px;'>";
                echo "<div class='card-body'>";
                echo "<h5 class='card-title'>Email : {$user['email']}</h5>";
                echo "<p class='card-text'>Nom : {$user['nom']}</p>";
                echo "<p class='card-text'>Prenom : {$user['prenom']}</p>";
                echo "<p class='card-text'>Naissance : {$user['naissance']}</p>";
                echo "<p class='card-text'>Adresse : {$user['adresse']}</p>";
                echo "<p class='card-text'>Telephone : {$user['telephone']}</p>";
                echo "<Form action='' method='POST'>";
                // Simuler les actions sur l'utilisateur
                echo "<button id='modal-kick' type='submit' name='action' value='block_{$user['email']}' class='btn btn-warning zoom-btn me-3'>Bloquer</button>";
                echo "<button id='modal-kick' type='submit' name='action' value='unblock_{$user['email']}' class='btn btn-warning zoom-btn me-3'>Debloquer</button>";
                echo "<button id='modal-del' type='submit' name='action' value='del_{$user['email']}' class='btn btn-danger zoom-btn'>Supprimer l'utilisateur</button>";
                echo "</Form>";
                echo "</div>";
                echo "</div>";
            }
            ?>
        </div>
        <div class="modal fade" id="control" tabindex="-1" aria-labelledby="controlLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="controlLabel">Ecran de controle</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class='card-body'>
                            <!-- Les données de l'utilisateur sélectionné seront affichées ici -->
                            <p>Email : <span id="modal-email"></span></p>
                            <p>Nom : <span id="modal-nom"></span></p>
                            <p>Prénom : <span id="modal-prenom"></span></p>
                            <p>Date de naissance : <span id="modal-naissance"></span></p>
                            <p>Adresse : <span id="modal-adresse"></span></p>
                            <p>Téléphone : <span id="modal-telephone"></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../script/script.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>

</html>
