<?php
class UserController{
    private $userModel;

    public function __construct(){
        require_once(dirname(__DIR__) . '/models/UserModel.php');
        $this->userModel = new UserModel();
    }

    public function getUser($email){
        return $this->userModel->getUser($email);
    }

    public function getAllUsers(){
        return $this->userModel->getAllUsers();
    }
}

