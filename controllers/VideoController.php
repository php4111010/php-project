<?php

class VideoController
{
    public function upload(){
        if ($_FILES["video"]["error"] > 0) {
            echo "Error during the upload : " . $_FILES["photo"]["error"] . "<br />";
            return null;
        } else {
            $uploadDir = dirname(__DIR__) . '/videos/';
            $uploadFile = $uploadDir . basename($_FILES['video']['name']);
            if(move_uploaded_file($_FILES['video']['tmp_name'], $uploadFile)){
                echo "File is valid, and was successfully uploaded.\n";
                return "../videos/".$_FILES['video']['name'];
            } else {
                echo "Error while uploading : " . $_FILES["video"]["error"] . "<br />";
                return null;
            }
        }
    }

    public function delete($file_path){
        unlink($file_path);
    }
}
?>